    //
    //  Coverage.cpp
    //  CameraCoverage
    //
    //  Created by Romain Kaddouch on 09/11/2014.
    //  Copyright (c) 2014 NONAME. All rights reserved.
    //

#include "Map.h"
#include "Constants.h"

const int Map::getDimX ()
{
    return dimX;
}

const int Map::getDimY ()
{
    return dimY;
}

const int Map::getNbCameras (void)
{
    return nbCameras;
}

const int Map::getNbUAVs (void)
{
    return nbUAVs;
}

const std::vector<Camera> Map::getCameraVect (void)
{
    return cameraVect;
}

std::vector<UAV> Map::getUavVect (void)
{
    return uavVect;
}

const bool Map::Empty(void)
{
    return isEmpty;
}


void Map::setX(int dimX)
{
    this->dimX = dimX;
}

void Map::setY(int y)
{
    dimY = y;
}

void Map::addObject (Camera camera)
{
    Camera tmp(camera.getX(),camera.getY(),camera.getVisibility(), camera.getAngle(), camera.getOrientation());
    cameraVect.push_back(tmp);
    nbCameras = nbCameras+1;
    isEmpty = false;
}

void Map::addObject (UAV uav)
{
    UAV tmp (uav.getID(),uav.getX(), uav.getY(), uav.getVisibility());
    uavVect.push_back(tmp);
    nbUAVs = nbUAVs+1;
    isEmpty = false;
}


Map Map::removeObject (Camera camera)
{
    return *new Map(this, camera);
    
}
Map Map::removeobject (UAV uav)
{
    return *new Map(this, uav);
    
}

void Map::removeObjectAndAdd(UAV uavDelete, UAV uavAdd)
{
    if (getUavVect().size() != 0)
    {
        for (std::vector<UAV>::iterator it = uavVect.begin() ; it != uavVect.end() ; ++it)
        {
            if (it->getVisibility() == uavDelete.getVisibility()
                && it->getX() == uavDelete.getX()
                && it->getY() == uavDelete.getY())
            {
                    //it->setX(uavAdd.getX());
                    //it->setY(uavAdd.getY());
                    // it->setID(uavAdd.getID());
                    // it->setVisibility(VISIBILITY);
            }
            
        }
        
    }
}


void Map::printCoverage()
{
    for (int i = 0; i < MAP_SIZE ; i++)
    {
        for (int j = 0 ; j< MAP_SIZE ; j++)
        {
            std::cout << coverage[i][j] << " ";
        }
        std::cout << std::endl;
    }
}





void Map::addRandomObstacles()
{
    int iObstacles= OBSTACLE_RATE * MAP_SIZE * MAP_SIZE;
    for (int i=0; i< iObstacles; i++)
    {
        int x = rand()%MAP_SIZE;
        int y = rand()%MAP_SIZE;
        if (coverage[x][y] != MAP_OBSTACLE)
        {
            coverage[x][y] = MAP_OBSTACLE;
        }
    }
}

void Map::addPredefinedObstacles()
{
    for (int x=0; x < MAP_SIZE; x++)
    {
        for (int y = 0 ; y< MAP_SIZE; y++)
        {
            if (y == MAP_SIZE*1/4 && x <= MAP_SIZE/2)
                coverage[y][x] = MAP_OBSTACLE;
            else if ((y < MAP_SIZE*3/16 || y > MAP_SIZE*5/16) && x == MAP_SIZE/2)
                coverage[y][x] = MAP_OBSTACLE;
            else if ((y < MAP_SIZE*5/16 || (y > MAP_SIZE*7/16 && y < MAP_SIZE*11/16) || y > MAP_SIZE*12/16) && x == MAP_SIZE*9/16)
                coverage[y][x] = MAP_OBSTACLE;
            else if ((y == MAP_SIZE*3/8 || y == MAP_SIZE*11/16) && x >= MAP_SIZE*9/16)
                coverage[y][x] = MAP_OBSTACLE;
            else if (y==0 || x == 0 || y == MAP_SIZE-1 || x == MAP_SIZE-1)
                coverage[y][x] = MAP_OBSTACLE;
            else
                coverage[y][x] = MAP;
        }
    }
}

void Map::reinitCoverage()
{
    for (int x=0; x < MAP_SIZE; x++)
        for (int y = 0 ; y< MAP_SIZE; y++)
            coverage[x][y] = 0;
    addPredefinedObstacles();
}


