//
//  Map.h
//  CameraCoverage
//
//  Created by Romain Kaddouch on 09/11/2014.
//  Copyright (c) 2014 NONAME. All rights reserved.
//

#ifndef __CameraCoverage__Map__
#define __CameraCoverage__Map__

#include <stdio.h>
#include <stdlib.h>

#include "Camera.h"
#include "UAV.h"

#include <gecode/driver.hh>
#include <gecode/int.hh>
#include <gecode/minimodel.hh>

#include <GLUT/GLUT.h>

using namespace Gecode;

class Map {
public:
    int dimX, dimY;
    int nbCameras;
    std::vector<Camera> cameraVect;
    int nbUAVs;
    std::vector<UAV> uavVect;
    bool isEmpty;
    

    int coverage[MAP_SIZE][MAP_SIZE];
    
    Map(void) : dimX(MAP_SIZE), dimY(MAP_SIZE), nbCameras(0), nbUAVs(0), isEmpty(true)
    {
            //Adding obstacles on map
        for (int i =0; i< MAP_SIZE; i++)
            for (int j=0; j< MAP_SIZE;j++)
                coverage[i][j] = MAP_UNKNOWN_AREA;
        if (OBSTACLES_RANDOM)
            addRandomObstacles();
        else if (OBSTACLES_PREDEFINED)
            addPredefinedObstacles();
    };
    
    Map (const Map* map, Camera camera):dimX(map->dimX), dimY(map->dimY), nbCameras(map->nbCameras), nbUAVs(map->nbUAVs), isEmpty((map->nbUAVs+map->nbCameras)? false : true){
        std::vector<Camera>::const_iterator iterator = getCameraVect().begin();
        std::vector<Camera>::const_iterator end = getCameraVect().end();
        do
        {
            if (iterator->getAngle() == camera.getAngle()
                && iterator->getOrientation() == camera.getOrientation()
                && iterator->getVisibility() == camera.getVisibility()
                && iterator->getX() == camera.getX()
                && iterator->getY() == camera.getY() );
            else
                addObject(*new Camera(iterator->getX(),iterator->getY(),iterator->getAngle(), iterator->getVisibility(), iterator->getOrientation()));
            iterator++;
        }while (iterator != end);
        
        std::vector<UAV>::const_iterator iteratorr = getUavVect().begin();
        std::vector<UAV>::const_iterator endd = getUavVect().end();
        int i=0;
        do
        {
            addObject(*new UAV(iteratorr->getID(), iteratorr->getX(),iterator->getY(), iteratorr->getVisibility()));
            iteratorr++;i++;
        }while (iteratorr != endd);
        
        for (int i=0; i< MAP_SIZE;i++)
            for (int j=0; j< MAP_SIZE;j++)
                coverage[i][j] = map->coverage[i][j];
        
    
    };
    
    Map (Map* map, UAV uav):dimX(map->dimX), dimY(map->dimY), nbCameras(map->nbCameras), nbUAVs(map->nbUAVs), isEmpty((map->nbUAVs+map->nbCameras)? false : true){
        std::vector<UAV>::const_iterator iteratorr = getUavVect().begin();
        std::vector<UAV>::const_iterator endd = getUavVect().end();
        if (getUavVect().size() != 0)
            do
            {
                if (iteratorr->getVisibility() == uav.getVisibility()
                    && iteratorr->getX() == uav.getX()
                    && iteratorr->getY() == uav.getY() );
                else
                    addObject(*new UAV(iteratorr->getID(),iteratorr->getX(),iteratorr->getY(), iteratorr->getVisibility()));
                
                iteratorr++;
            }while (iteratorr != endd);
        
        
        
        std::vector<Camera>::const_iterator iterator = getCameraVect().begin();
        std::vector<Camera>::const_iterator end = getCameraVect().end();
        if (getCameraVect().size() != 0)
            do
            {
                addObject(*new Camera(iterator->getX(),iterator->getY(),iterator->getAngle(), iterator->getVisibility(), iterator->getOrientation()));
                iterator++;
            }while (iterator != end);
        
        for (int i=0; i< MAP_SIZE;i++)
            for (int j=0; j< MAP_SIZE;j++)
                coverage[i][j] = map->coverage[i][j];
            //map = new Map();
        
    };
    
    Map (Map* map, UAV uavDelete, UAV uavAdd):dimX(map->dimX), dimY(map->dimY), nbCameras(map->nbCameras), nbUAVs(map->nbUAVs), isEmpty((map->nbUAVs+map->nbCameras)? false : true){
        int toRemove = -1;
        std::vector<UAV>::const_iterator iteratorr = getUavVect().begin();
        std::vector<UAV>::const_iterator endd = getUavVect().end();
        if (getUavVect().size() != 0)
        { 
            for (std::vector<UAV>::iterator it = getUavVect().begin() ; it != getUavVect().end() ; ++it)
            {
                if (iteratorr->getVisibility() == uavDelete.getVisibility()
                    && iteratorr->getX() == uavDelete.getX()
                    && iteratorr->getY() == uavDelete.getY())
                {
                    it->setX(uavAdd.getX());
                    it->setY(uavAdd.getY());
                    it->setID(uavAdd.getID());
                    it->setVisibility(VISIBILITY);
                }

            }
            
        }
        
        
        
        
        std::vector<Camera>::const_iterator iterator = getCameraVect().begin();
        std::vector<Camera>::const_iterator end = getCameraVect().end();
        if (getCameraVect().size() != 0)
            do
            {
                addObject(*new Camera(iterator->getX(),iterator->getY(),iterator->getAngle(), iterator->getVisibility(), iterator->getOrientation()));
                iterator++;
            }while (iterator != end);
        
        for (int i=0; i< MAP_SIZE;i++)
            for (int j=0; j< MAP_SIZE;j++)
                coverage[i][j] = map->coverage[i][j];
            //map = new Map();
        
    };
    
    Map (Map* map):dimX(map->dimX), dimY(map->dimY), nbCameras(map->nbCameras), nbUAVs(map->nbUAVs), isEmpty((map->nbUAVs+map->nbCameras)? false : true){
       
        std::vector<UAV>::const_iterator iteratorr = getUavVect().begin();
        std::vector<UAV>::const_iterator endd = getUavVect().end();
        if (getUavVect().size() != 0)
            do
            {
                addObject(*new UAV(iteratorr->getID(),iteratorr->getX(),iteratorr->getY(), iteratorr->getVisibility()));
                iteratorr++;
            }while (iteratorr != endd);

        std::vector<Camera>::const_iterator iterator = getCameraVect().begin();
        std::vector<Camera>::const_iterator end = getCameraVect().end();
        if (getCameraVect().size() != 0)
            do
            {
                addObject(*new Camera(iterator->getX(),iterator->getY(),iterator->getAngle(), iterator->getVisibility(), iterator->getOrientation()));
                iterator++;
            }while (iterator != end);
        
        for (int i=0; i< MAP_SIZE;i++)
            for (int j=0; j< MAP_SIZE;j++)
                coverage[i][j] = map->coverage[i][j];
            //map = new Map();
        
    };
    
    ~Map(void){};
    
    
    const int getDimX ();
    const int getDimY ();
    const int getNbCameras ();
    const int getNbUAVs ();
    const std::vector<Camera> getCameraVect (void);
    std::vector<UAV> getUavVect (void);
    const bool Empty(void);
    
    
    void setX(int x);
    void setY(int y);
    
    void addObject (Camera camera);
    void addObject (UAV uav);
    
    Map removeObject (Camera camera);
    Map removeobject (UAV uav);
    void removeObjectAndAdd(UAV uavDelete, UAV uavAdd);
    
    void printCoverage(void);
    
    void addRandomObstacles(void);
    void addPredefinedObstacles(void);
    void reinitCoverage(void);
    
};


#endif /* defined(__CameraCoverage__Map__) */
