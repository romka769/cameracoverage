//
//  PrintObjects.h
//  CameraCoverage
//
//  Created by Romain Kaddouch on 01/12/2014.
//  Copyright (c) 2014 NONAME. All rights reserved.
//

#ifndef CameraCoverage_PrintObjects_h
#define CameraCoverage_PrintObjects_h

#include <stdio.h>
#include <cmath>

#include "Constants.h"
#include "Map.h"
#include "UAV.h"

#include <GLUT/GLUT.h>

double ToScreen(double coord);
double ToRad(int angle);

bool epsequal (double A, double B);

bool isInsideMap(double x);

int printLineOfSight (double x1, double y1, double x2, double y2, Map &tmp);
double valueVisibilityArea (double x1, double y1, double x2, double y2, Map &tmp, int (&map)[MAP_SIZE][MAP_SIZE]);

void printObject (Camera cam, Map *tmp, int rotationAngle);
void printObject (UAV uav, Map *tmp);
void fillCoverage (UAV uav, Map *tmp);
#endif
