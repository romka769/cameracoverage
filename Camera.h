//
//  Camera.h
//  CameraCoverage
//
//  Created by Romain Kaddouch on 12/11/2014.
//  Copyright (c) 2014 NONAME. All rights reserved.
//

#ifndef __CameraCoverage__Camera__
#define __CameraCoverage__Camera__

#include <stdio.h>
#include <stdlib.h>

#include "Constants.h"

#include <gecode/driver.hh>
#include <gecode/int.hh>
#include <gecode/set.hh>
#include <gecode/minimodel.hh>

#include <GLUT/glut.h>

using namespace Gecode;


class Camera {
private:

    
public:
    int posX, posY;
    int coverageAngle;
    int visibilityRange;
    int orientationAngle;
    int angleMin;
    int angleMax;
    
    
        //Constructors
    Camera(void) : posX(0), posY(0), coverageAngle(90), visibilityRange(3), orientationAngle(270){};
    Camera(int x, int y, int angle, int visibility) : posX(x), posY(y), coverageAngle(angle), visibilityRange(visibility){
        if (x==0 && y==0)
            orientationAngle = 270;
        else if (x==0 && y ==MAP_SIZE)
            orientationAngle = 0;
        else if(x==MAP_SIZE && y==0)
            orientationAngle = 180;
        else if(x==MAP_SIZE && y==MAP_SIZE)
            orientationAngle = 90;
        else
            orientationAngle = 0;
    }
    Camera(int x, int y, int angle, int visibility, int orientation) : posX(x), posY(y), coverageAngle(angle), visibilityRange(visibility), orientationAngle(orientation){
    };
    
    Camera(Camera const &camera) : posX(camera.getX()), posY(camera.getY()), coverageAngle(camera.getAngle()), visibilityRange(camera.getVisibility()), orientationAngle(camera.getOrientation())
    {};
    
        //Destructor
    ~Camera (void){};
    
        //Overload operator =
    
        //Getters
    int const& getX(void) const;
    int const& getY(void) const;
    int const& getAngle(void) const;
    int const& getVisibility(void) const;
    int const& getOrientation(void) const;
    
        //Setters
    void setX(int x);
    void setY(int y);
    void setAngle(int angle);
    void setVisibility(int visibility);
    
        //Print - flow insertion
    void print(std::ostream& os) const;
};


#endif /* defined(__CameraCoverage__Camera__) */
