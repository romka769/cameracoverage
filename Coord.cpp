//
//  Coord.cpp
//  CameraCoverage
//
//  Created by Romain Kaddouch on 16/12/2014.
//  Copyright (c) 2014 NONAME. All rights reserved.
//

#include "Coord.h"

const int Coord::getX (void)
{
    return x;
}


const int Coord::getY (void)
{
    return y;
}
