//
//  PrintObjects.cpp
//  CameraCoverage
//
//  Created by Romain Kaddouch on 01/12/2014.
//  Copyright (c) 2014 NONAME. All rights reserved.
//

#include "PrintObjects.h"

double ToScreen(double coord)
{
    return (coord*(WINDOW_SIZE/MAP_SIZE));
}

double ToRad(int angle)
{
    return (angle*M_PI/180);
}

bool epsequal (double A, double B)
{
    float diff = A - B;
    return (diff < EPSILON) || (-diff < EPSILON);
}

bool isInsideMap(double x)
{
    return (x >= 0 && x <= MAP_SIZE);
}

int printLineOfSight (double x1, double y1, double x2, double y2, Map &tmp)
{
    int area =0;
    double dx = (x2-x1);
    double dy = (y2-y1);
    
    double x = x1, y = y1;
    
    for (int i =1; i < ITERATIONS_SIGHT; i++)
    {
        if (isInsideMap(x) && isInsideMap(y))
        {
            if (tmp.coverage[(int)x][(int)y] == MAP_OBSTACLE)
            {
                i=ITERATIONS_SIGHT;
            }
            else if (epsequal((fmod(x, 1.0)), 0.5) && epsequal((fmod(y, 1.0)), 0.5))
            {
                tmp.coverage[(int)x][(int)y] = MAP_VIEWED_AREA;
                area++;
            }
            x+=(dx/ITERATIONS_SIGHT);
            y+=(dy/ITERATIONS_SIGHT);
        }
        else
            i=ITERATIONS_SIGHT;
    }
    return area;
}

double valueVisibilityArea (double x1, double y1, double x2, double y2, Map &tmp, int (&map)[MAP_SIZE][MAP_SIZE])
{
    double area =0.0;
    double iterations =0.0;
    double dx = (x2-x1);
    double dy = (y2-y1);
    
    double x = x1, y = y1;
    
    for (int i =0; i < ITERATIONS_SIGHT; i++)
    {
        if (isInsideMap(x) && isInsideMap(y))
        {
            if (tmp.coverage[(int)x][(int)y] == MAP_OBSTACLE)
            {
                i=ITERATIONS_SIGHT;
            }
            else if (epsequal((fmod(x, 1.0)), 0.5) && epsequal((fmod(y, 1.0)), 0.5))
            {
                area+= map[(int)x][(int)y];
                iterations+=1.0;
            }
            x+=(dx/ITERATIONS_SIGHT);
            y+=(dy/ITERATIONS_SIGHT);
        }
        else
            i=ITERATIONS_SIGHT;
    }
    if (iterations != 0.0)
        return (area*1.0/iterations);
    else
        return 0;
}

void printObject (Camera cam, Map *tmp, int rotationAngle)
{
    int totalArea=0;
    
    glColor3d(0.95f,0.0f,0.0f);
    glRectf(ToScreen(cam.getX())-5, ToScreen(cam.getY())-5, ToScreen(cam.getX())+5, ToScreen(cam.getY())+5);
    glLineWidth(2.0);
    glColor3d(1.0, 0.0, 0.0);
    
    
    glBegin(GL_LINES);
    glVertex3f(ToScreen(cam.getX()), ToScreen(cam.getY()), 0);
    glVertex3f(ToScreen((cam.getX()+cam.getVisibility()*cos(ToRad(cam.getOrientation()+cam.getAngle()+rotationAngle)))),ToScreen( cam.getY()-cam.getVisibility()*sin(ToRad(cam.getOrientation()+cam.getAngle()+rotationAngle))), 0);
    glEnd();
    
    glBegin(GL_LINES);
    glVertex3f(ToScreen(cam.getX()), ToScreen(cam.getY()), 0);
    glVertex3f(ToScreen((cam.getX()+cam.getVisibility()*cos(ToRad(cam.getOrientation()+rotationAngle)))),ToScreen( cam.getY()-cam.getVisibility()*sin(ToRad(cam.getOrientation()+rotationAngle))), 0);
    glEnd();
    
    
    for(double angle=cam.getOrientation()+rotationAngle;angle<=cam.getOrientation()+cam.getAngle()+rotationAngle;angle+=1.2)
    {
        double vectorX=cam.getX()+(cam.getVisibility()*cos(ToRad(angle)));
        double vectorY=cam.getY()-(cam.getVisibility()*sin(ToRad(angle)));
        glVertex2d(ToScreen(vectorX),ToScreen(vectorY));
        totalArea += printLineOfSight(cam.getX(), cam.getY(), vectorX, vectorY, *tmp);
    }
}

void printObject (UAV uav, Map *tmp)
{
    int totalArea=0;
    
    glColor3d(0.95f,0.0f,0.0f);
    glRectf(ToScreen(uav.getX())-5, ToScreen(uav.getY())-5, ToScreen(uav.getX())+5, ToScreen(uav.getY())+5);
    glLineWidth(2.0);
    glColor3d(1.0, 0.0, 0.0);
    
    for(double angle=0;angle<=360;angle+=1.2)
    {
        double vectorX=uav.getX()+(uav.getVisibility()*cos(ToRad(angle)));
        double vectorY=uav.getY()-(uav.getVisibility()*sin(ToRad(angle)));
        glVertex2d(ToScreen(vectorX),ToScreen(vectorY));
        totalArea += printLineOfSight(uav.getX(), uav.getY(), vectorX, vectorY, *tmp);
    }
}

void fillCoverage (UAV uav, Map *tmp)
{
    for(double angle=0;angle<=360;angle+=1.2)
    {
        double vectorX=uav.getX()+(uav.getVisibility()*cos(ToRad(angle)));
        double vectorY=uav.getY()-(uav.getVisibility()*sin(ToRad(angle)));
        printLineOfSight(uav.getX(), uav.getY(), vectorX, vectorY, *tmp);
    }
}
