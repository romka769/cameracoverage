//
//  Camera.cpp
//  CameraCoverage
//
//  Created by Romain Kaddouch on 12/11/2014.
//  Copyright (c) 2014 NONAME. All rights reserved.
//

#include "Camera.h"

    // Getters
int const& Camera::getX(void) const
{
    return posX;
}

int const& Camera::getY(void) const
{
    return posY;
}

int const& Camera::getAngle(void) const
{
    return coverageAngle;
}

int const& Camera::getVisibility(void) const
{
    return visibilityRange;
}



int const& Camera::getOrientation(void) const
{
    return orientationAngle;
}

    // Setters
void Camera::setX(int x)
{
    posX = x;
}

void Camera::setY(int y)
{
    posY = y;
}

void Camera::setAngle(int angle)
{
    coverageAngle = angle;
}

void Camera::setVisibility(int visibility)
{
    visibilityRange = visibility;
}

    //print
void Camera::print(std::ostream& os) const {
    os << "Camera : " << " Posx : " << posX << " Posy : " << posY << " coverage angle : " << coverageAngle << " visibility range : " << visibilityRange << std::endl;
}
