//
//  Constants.h
//  CameraCoverage
//
//  Created by Romain Kaddouch on 12/11/2014.
//  Copyright (c) 2014 NONAME. All rights reserved.
//

#ifndef CameraCoverage_Constants_h
#define CameraCoverage_Constants_h


#define SMALL 10
#define MEDIUM 20
#define LARGE 160

#define MAP_SIZE LARGE
#define WINDOW_SIZE 800

#define OBSTACLE_RATE 0.1

#define MAP 0
#define MAP_OBSTACLE 1
#define MAP_CAMERA 2
#define MAP_VIEWED_AREA 3
#define MAP_UNKNOWN_AREA 4
#define MAP_SPOT_FULL_COVERAGE 5
#define MAP_TEST 10

#define EPSILON 0.5

#define ITERATIONS_SIGHT 200

#define OBSTACLES_RANDOM 0
#define OBSTACLES_PREDEFINED 1

#define ROTATION 0

#define ROTATION_SPEED 3

#define MAX_VALUE_MAP 500

#define VISIBILITY 15
    // if VISIBILITY == 15, NB_SPOTS == 87


#define NB_SPOTS 87



#endif
