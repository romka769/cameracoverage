//
//  UAV.h
//  CameraCoverage
//
//  Created by Romain Kaddouch on 01/12/2014.
//  Copyright (c) 2014 NONAME. All rights reserved.
//

#ifndef __CameraCoverage__UAV__
#define __CameraCoverage__UAV__


#include <stdio.h>
#include <stdlib.h>

#include "Constants.h"

#include <gecode/driver.hh>
#include <gecode/int.hh>
#include <gecode/set.hh>
#include <gecode/minimodel.hh>


using namespace Gecode;


class UAV {
private:
    int ID;
    int posX, posY;
    int visibilityRange;
    
    
public:

    
    
        //Constructors
    UAV(void) : posX(0), posY(0), visibilityRange(3){};
    UAV(int ID, int x, int y, int visibility) : ID(ID), posX(x), posY(y), visibilityRange(visibility){};
    
    UAV(UAV const &UAV) : ID(UAV.getID()), posX(UAV.getX()), posY(UAV.getY()), visibilityRange(UAV.getVisibility())
    {};
    
        //Destructor
    ~UAV (void){};
    
    
    
        //Getters
    int const& getID(void) const;
    int const& getX(void) const;
    int const& getY(void) const;
    int const& getVisibility(void) const;
    
        //Setters
    void setID(const int x);
    void setX(const int x);
    void setY(const int y);
    void setVisibility(const int visibility);
    
    void Offset(int dx, int dy);
    
    
        //Print - flow insertion
    void print(std::ostream& os) const;
};


#endif /* defined(__CameraCoverage__UAV__) */
