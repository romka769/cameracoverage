//
//  UAV.cpp
//  CameraCoverage
//
//  Created by Romain Kaddouch on 01/12/2014.
//  Copyright (c) 2014 NONAME. All rights reserved.
//

#include "UAV.h"

    // Getters
int const& UAV::getID(void) const
{
    return ID;
}

int const& UAV::getX(void) const
{
    return posX;
}

int const& UAV::getY(void) const
{
    return posY;
}

int const& UAV::getVisibility(void) const
{
    return visibilityRange;
}

    // Setters
void UAV::setID(const int id)
{
    this->ID = id;
}

void UAV::setX(const int x)
{
    this->posX = x;
}

void UAV::setY(const int y)
{
    this->posY = y;
}

void UAV::setVisibility(const int visibility)
{
    visibilityRange = visibility;
}

void UAV::Offset(int dx, int dy)
{
    this->setX(30);
    this->setY(30);
}


    //print
void UAV::print(std::ostream& os) const {
    os << "UAV : " << " Posx : " << posX << " Posy : " << posY << " visibility range : " << visibilityRange << std::endl;
}
