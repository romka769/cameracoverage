//
//  Coord.h
//  CameraCoverage
//
//  Created by Romain Kaddouch on 16/12/2014.
//  Copyright (c) 2014 NONAME. All rights reserved.
//

#ifndef __CameraCoverage__Coord__
#define __CameraCoverage__Coord__

#include <stdio.h>

class Coord {
    private :
    int x;
    int y;
    public :
    Coord (int x, int y) : x(x), y(y)
    {
        ;
    };
    
    Coord () : x(0), y(0)
    {
        ;
    };
    
    const int getX (void);
    const int getY (void);
};
#endif /* defined(__CameraCoverage__Coord__) */
